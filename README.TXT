
    This is the bitmap filters component of a larger private
    game engine project I am working on called "MKB" .

    If this is me ( KANJICODER ) reading this file right
    now , you need to make sure that you work in :

    +------------------------------------------------------+
    |                                                      |
    |   https://gitlab.com/KANJICODER/MKB.git              |
    |   S:\R\G\MKB\DEP\IMG2024\IMG2024.C11                 |
    |                                                      |
    +------------------------------------------------------+

    If this is anyone else , this code is used in a youtube
    series that shows you the low level algorithm logic
    for writing image filters .

    The series is on my main youtube channel :

    www.youtube.com/@KANJICODER

    __TODO__YOUTUBE__PLAYLIST__URL__HERE__